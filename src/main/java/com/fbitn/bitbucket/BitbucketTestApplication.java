package com.fbitn.bitbucket;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import lombok.AllArgsConstructor;
import lombok.Data;

@SpringBootApplication
@RestController
public class BitbucketTestApplication {

	public static void main(String[] args) {
		SpringApplication.run(BitbucketTestApplication.class, args);
	}
	
	@GetMapping("/")
	public ResponseEntity<Greeting> hello(@RequestParam(name="name", required=false, defaultValue="World") String name) {		
				
		return ResponseEntity.ok().body(new Greeting("Hello " + name + "!"));
	}
}

@Data
@AllArgsConstructor
class Greeting {
	private String content;
}
